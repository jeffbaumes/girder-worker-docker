# Assumes a locally built girder_worker
docker pull rabbitmq:management
docker pull mongo:latest
docker pull girder/girder:latest

docker network create girder

docker run -d --name rmq --network girder rabbitmq:management
docker run -d --name girder_worker --network girder -v /tmp/girder_worker:/tmp/girder_worker -v /usr/bin/docker:/usr/bin/docker -v /var/run/docker.sock:/var/run/docker.sock --privileged girder_worker --broker amqp://rmq
docker run -d --name mongo --network girder -v $(pwd)/mongodata:/data/db:rw mongo:latest
docker run -d --name girder --network girder -p 8080:8080 girder/girder:latest --database mongodb://mongo/girder

# Provision
ansible-playbook -i localhost, -c local -M /etc/ansible/roles/girder.girder/library site.yml
docker exec -i -t girder_worker /bin/bash -c 'sudo addgroup --gid=`stat -c %g /var/run/docker.sock` dockerhost && sudo adduser worker dockerhost'
docker restart girder_worker
