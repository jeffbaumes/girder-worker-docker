To build girder_worker's docker image based off a girder_worker checkout:

```
cd /path/to/girder_worker
cp /path/to/girder-worker-docker/Dockerfile-girder-worker ./Dockerfile
docker build -t girder_worker .
```

To start the containers:
```
pip install ansible
ansible-galaxy install girder.girder
mkdir /tmp/girder_worker
cd /path/to/girder-worker-docker
mkdir ./mongodata
./start.sh
```

To stop the containers:
```
./stop.sh
```
