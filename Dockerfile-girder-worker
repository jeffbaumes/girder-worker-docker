FROM node:6

RUN mkdir /girder_worker

RUN apt-get update && \
  apt-get install -qy software-properties-common python-software-properties && \
  apt-get update && apt-get install -qy \
    sudo \
    build-essential \
    wget \
    python \
    libffi-dev \
    libssl-dev \
    libjpeg-dev \
    zlib1g-dev \
    libpython-dev && \
  apt-get clean && rm -rf /var/lib/apt/lists/*

RUN wget https://bootstrap.pypa.io/get-pip.py && python get-pip.py

WORKDIR /girder_worker
COPY setup.py /girder_worker/setup.py
COPY requirements.txt /girder_worker/requirements.txt
COPY README.rst /girder_worker/README.rst
COPY examples /girder_worker/examples
COPY scripts /girder_worker/scripts
COPY girder_worker /girder_worker/girder_worker

RUN pip install -e .[docker,girder_io]
RUN girder-worker-config set girder_worker plugins_enabled docker,girder_io
RUN girder-worker-config set girder_worker tmp_root /tmp/girder_worker

RUN adduser --disabled-password --gecos '' worker && \
  adduser worker sudo && \
  echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

RUN chown worker /girder_worker
RUN chgrp worker /girder_worker

USER worker

ENTRYPOINT ["python", "-m", "girder_worker"]
